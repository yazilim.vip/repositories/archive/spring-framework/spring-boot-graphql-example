package vip.yazilim.graphql.example.mapper;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import vip.yazilim.graphql.example.dto.HumanCreateRequestDto;
import vip.yazilim.graphql.example.dto.HumanResponseDto;
import vip.yazilim.graphql.example.entity.Human;

/**
 * @author Emre Şen (maemresen@yazilim.vip), 24/04/2022
 */
@Mapper(
    componentModel = "spring",
    builder = @Builder(disableBuilder = true)
)
public interface HumanMapper {
    HumanResponseDto toHumanResponseDto(Human human);

    Human toEntity(HumanCreateRequestDto humanRequestDto);
}

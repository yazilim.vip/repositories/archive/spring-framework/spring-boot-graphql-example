package vip.yazilim.graphql.example;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import vip.yazilim.graphql.example.entity.Human;
import vip.yazilim.graphql.example.repository.HumanRepository;

/**
 * @author Emre Şen (maemresen@yazilim.vip), 23/04/2022
 */
@RequiredArgsConstructor
@Component
public class Initializer implements CommandLineRunner {

    private final HumanRepository humanRepository;

    @Override
    public void run(String... args) throws Exception {
        humanRepository.save(Human.builder()
                .name("Emre")
                .surname("Şen")
                .age(25)
                .build());
        humanRepository.save(Human.builder()
                .name("Ahmet")
                .surname("Yılmaz")
                .age(30)
                .build());
    }
}
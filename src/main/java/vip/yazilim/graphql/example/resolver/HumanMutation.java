package vip.yazilim.graphql.example.resolver;

import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.InputArgument;
import lombok.RequiredArgsConstructor;
import vip.yazilim.graphql.example.dto.HumanCreateRequestDto;
import vip.yazilim.graphql.example.dto.HumanResponseDto;
import vip.yazilim.graphql.example.entity.Human;
import vip.yazilim.graphql.example.mapper.HumanMapper;
import vip.yazilim.graphql.example.repository.HumanRepository;

/**
 * @author Emre Şen (maemresen@yazilim.vip), 24/04/2022
 */
@RequiredArgsConstructor
@DgsComponent
public class HumanMutation {

    private final HumanRepository humanRepository;
    private final HumanMapper humanMapper;

    @DgsData(parentType = "Mutation", field = "createHuman")
    public HumanResponseDto createHuman(@InputArgument HumanCreateRequestDto humanToCreate) {
        String name = humanToCreate.getName();
        String surname = humanToCreate.getSurname();
        Integer age = humanToCreate.getAge();
        if (age == null) {
            age = 20;
        }
        Human human = Human.builder()
            .name(name)
            .surname(surname)
            .age(age)
            .build();
        Human savedHuman = humanRepository.save(human);
        return humanMapper.toHumanResponseDto(savedHuman);
    }
}
package vip.yazilim.graphql.example.resolver;

import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsQuery;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import vip.yazilim.graphql.example.dto.HumanResponseDto;
import vip.yazilim.graphql.example.mapper.HumanMapper;
import vip.yazilim.graphql.example.repository.HumanRepository;

/**
 * @author Emre Şen (maemresen@yazilim.vip), 23/04/2022
 */
@DgsComponent
@RequiredArgsConstructor
public class HumanDataFetcher {

    private final HumanRepository humanRepository;
    private final HumanMapper humanMapper;

    @DgsQuery
    public List<HumanResponseDto> findAllHumans() {
        return humanRepository.findAll().stream().map(humanMapper::toHumanResponseDto).collect(Collectors.toList());
    }
}

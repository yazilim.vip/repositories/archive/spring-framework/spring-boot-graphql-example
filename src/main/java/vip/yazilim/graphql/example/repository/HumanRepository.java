package vip.yazilim.graphql.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vip.yazilim.graphql.example.entity.Human;

/**
 * @author Emre Şen (maemresen@yazilim.vip), 23/04/2022
 */
@Repository
public interface HumanRepository extends JpaRepository<Human, Long> {

}

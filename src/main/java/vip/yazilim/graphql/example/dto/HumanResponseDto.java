package vip.yazilim.graphql.example.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Emre Şen (maemresen@yazilim.vip), 24/04/2022
 */
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class HumanResponseDto {
    private String name;
    private String surname;
    private Integer age;
}
